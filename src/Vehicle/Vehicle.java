package Vehicle;
abstract class Vehicle {
    public abstract void startEngine();
    public abstract void drive();
}

class Car extends Vehicle {
    @Override
    public void startEngine() {
        System.out.println("Car is starting.");
    }

    @Override
    public void drive() {
        System.out.println("Car is driving.");
    }
}

class Motorcycle extends Vehicle {
    @Override
    public void startEngine() {
        System.out.println("Motorcycle is starting.");
    }

    @Override
    public void drive() {
        System.out.println("Motorcycle is driving.");
    }
}

class Truck extends Vehicle {
    @Override
    public void startEngine() {
        System.out.println("Truck engine is starting.");
    }

    @Override
    public void drive() {
        System.out.println("Truck is driving.");
    }
}