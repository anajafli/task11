package Student;

public class Main {
    public static void main(String[] args) {
        Student student = new Student();

        student.setName("Afat Najafli");
        student.setAge(23);
        student.setGrade(91);
        student.displayInfo();
    }
}
